'use strict'

import express from 'express'
import expressValidator from 'express-validator'
import morgan from 'morgan'

import config from './appConfig'
import logger from './models/logger'
import swaggerUi from 'swagger-ui-express'

import swaggerDocument from '../swagger.json'
import mainRouter from './routes/mainRouter.js'

const app = express()

app.use((req, res, next) => {
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", '*');
        res.header("Access-Control-Allow-Credentials", true);
        res.header('Access-Control-Allow-Methods', 'GET, POST');
        res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
        next();
    });
    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'POST, GET')
        return res.status(200)
            .json({})
    } else next()
})

app.use(express.json())
app.use(expressValidator())

if (config.status == 'dev') {
    app.use(morgan('dev'))
} else {
    app.use(morgan('tiny'))
}

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
app.use('/api/v1', mainRouter)

app.use((req, res, next) => {
    const newError = new Error('Not Found')
    newError.status = 404
    next(newError)
})

app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.json({
        error: {
            message: err.message
        }
    })
})

export default app

export const startApp = async (port) => {
    port = port || config.port
    app.listen(port, () => {
        logger.debug(`Web server listening on: ${port}`)
        logger.debug(`Click to visit api here ->: http://localhost:${port}/api/v1`)
        logger.debug(`Or docs here ->: http://localhost:${port}/api-docs`)
    })
}
