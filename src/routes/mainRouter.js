import { Router } from 'express'

import * as userController from '../controllers/messageController'
import { validate } from '../controllers/validator'
const router = Router()

router.post('/messages',
    validate('addMessage'),
    userController.validationHandler,
    userController.addMessage)

router.get('/messages/list/:pageNum',
    validate('getMessages'),
    userController.validationHandler,
    userController.getMessages)

router.get('/messages/single/:mesId',
    validate('getMessage'),
    userController.validationHandler,
    userController.getMessage)

export default router
