'use strict'

import { body, validationResult, param } from 'express-validator/check'
import logger from '../models/logger'

const getMessagesValidator = () => {
    return [
        param('pageNum', 'mesId doesn`t exists')
        .exists(),
        param('pageNum', 'pageNum must be an integer and greater than 0')
        .isInt({ min: 0 })
    ]
}

const getMessageValidator = () => {
    return [
        param('mesId', 'mesId doesn`t exists')
        .exists()
    ]
}

const addMessageValidator = () => {
    return [
        body('text', 'text doesn`t exists')
        .exists(),
        body('text', 'text must be in length 1-100')
        .isLength({ min: 1, max: 99 }),
        body('email', 'Invalid email')
        .exists()
        .isEmail(),
    ]
}

export const validate = (validationMethod) => {
    switch (validationMethod) {
    case 'addMessage': {
        return addMessageValidator()
    }
    case 'getMessage': {
        return getMessageValidator()
    }
    case 'getMessages': {
        return getMessagesValidator()
    }
    }
}
