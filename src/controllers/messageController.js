'use strict'

import { validationResult } from 'express-validator/check'

import * as messageModel from '../models/messageModel'
import logger from '../models/logger'

export const addMessage = async (req, res, next) => {
    const { text, email } = req.body
    messageModel.addMessage(text, email)
        .then(newlyInserted => {
            sendSuccess(res, newlyInserted)
        })
        .catch(err => {
            logger.error(err)
            next(new Error('Unexpected Error'))
        })
}

export const getMessage = async (req, res, next) => {
    const { mesId } = req.params
    const message = await messageModel.getMessage(mesId)
    if (message) {
        sendSuccess(res, message)
    } else {
        const httpError = new Error(`Invalid ID supplied`)
        httpError.status = 404
        next(httpError)
    }
}

export const getMessages = async (req, res, next) => {
    const { pageNum } = req.params
    const messages = await messageModel.getMessages(pageNum)
    if (messages.length > 0) {
        sendSuccess(res, messages)
    } else {
        const httpError = new Error(`Messages not found`)
        httpError.status = 404
        next(httpError)
    }
    console.log(messages)
}

export const validationHandler = (req, res, next) => {
    const validationError = validationResult(req)
    if (!validationError.isEmpty()) {
        const httpError = new Error(`Invalid input`)
        httpError.status = 400
        throw httpError
    } else next()
}

function sendSuccess(res, object) {
    object = object || { description: 'successful operation' }
    res
        .status(200)
        .json(object)
}
