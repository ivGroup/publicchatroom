'use strict'

import * as dbController from '../db/dbController'
import logger from './logger';


export const addMessage = async (text, email) => {
    const newMessage = { text, email }
    const result = await dbController.message.add(newMessage)
    const newlyInserted = await dbController.message.byId(result.insertedId)
    return newlyInserted
}

export const getMessage = async (mesId) => {
    try {
        const message = await dbController.message.byId(String(mesId))
        return message
    } catch (err) {
        return null
    }
}

export const getMessages = async (pageNum) => {
    try {
        const messages = await dbController.message.getPagination(pageNum, 2)
        return messages
    } catch (err) {
        return []
    }
}
