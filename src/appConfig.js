'use strict'

const config = {}

config.port = process.env.PORT || 5000
config.status = process.env.status || 'prod'
config.databaseURL = process.env.databaseURL

export default config
