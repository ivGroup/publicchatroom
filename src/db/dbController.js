'use strict'

import mongoose from 'mongoose'

import logger from '../models/logger'
import config from '../appConfig'

import * as message from './message'

export { message }

mongoose.Promise = global.Promise
mongoose.connect(config.databaseURL, {
        useNewUrlParser: true
    })
    .catch(e => {
        logger.error('Mongo Error->' + e)
    })

const db = mongoose.connection
