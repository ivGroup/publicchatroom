'use strict'

import mongoose from 'mongoose'
import mongoose_validator from 'mongoose-validator'

const textValidator = [
    mongoose_validator({
        validator: 'isLength',
        arguments: [1, 100],
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
    })
]

const dataSchema = mongoose.Schema({
    authorized: { type: Boolean, default: false },
    email: {
        type: String,
        required: true,
        trim: true,
        // unique: true,
        match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    },
    text: {
        type: String,
        required: true,
        validate: textValidator
    },
    updatedDate: {
        type: Date,
        default: Date.now,
        auto: true
    },
    createDate: {
        type: Date,
        default: Date.now,
        set: (val) => this.securedField,
        auto: true
    }
})

const Model = mongoose.model('messages', dataSchema)

export default Model

export async function byId(id) {
    return Model.findOne({ "_id": id })
}

export async function add(data) {
    data.authorized = false
    data.createDate = new Date()
    return Model.collection.insertOne(data)
}

export async function getPagination(numPage, perPage) {
    return Model.find({})
        .limit(perPage)
        .skip(numPage * perPage)
        .sort({ 'createDate': -1 })

}
