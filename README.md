# publicChatRoom

Node.js application - public chat room(only RESTful API).

Unauthenticated users can post messages in chat so others can read them.
Messages need to be saved to the database.